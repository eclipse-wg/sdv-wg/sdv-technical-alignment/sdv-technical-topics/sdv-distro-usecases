
# SDV Distors and Usecases - Minutes of Meeting
--------------------

## Date: 2023-03-09 11:30 CET
## Place:
    SDV Use cases & Distros
    Thursday, 11:30-12:30
    Google Meet joining info
    Video call link: https://meet.google.com/ghm-ezpg-nuz
    Or dial: (GB) +44 20 3937 0880 PIN: 207 998 608#
    More phone numbers: https://tel.meet/ghm-ezpg-nuz?pin=9603275677533

## Participants:
Kai, Florian, Naci, Stefan, Hans-Dirk, Mike/Briefly

## Notes:
Status of the showcases:
- FMS -
  - Kai is working on integrating Charriot with the existing FMS demo. Not going as fast as would have like it to.
  - Need to understand more about the correct scenario for charriot integration.
  - Need feedback from @charriot
  - Plans to run the demo on a general purpose linux
  - Will need to work with @leda to create layer for Yocto
- ROS -
  - Could not workput a real life scnarios for ROS/Kuksa/Velocitas path yet, will probably drop it, but they will try to get feedback from @kuksa na @velocitas first
  - Plans to run the demo with docker
  - A strip down version maybe able to run on a beefed up RPi or Yocto
- eCal -
Florian investigated risc5 for eCal. There is an ubuntu beta for risc5 so it may be possible. Stefan believes there is a bright future for risc5 in automotive.

