# SDV Distro Use Cases

In preparation to define the list of use cases and the scope/content of the SDV distro(s) we are starting a breakout discussion group.  The expectation is to have the SDV project leaders or someone representing each project to participate in these meetings.  The expected outputs of these meetings are:

- Define and publish a set of  SDV use-cases highlighting:
  - The capabilities and features of software provided by SDV projects
  - Explore potential collaboration and integration of SDV projects
- SDV Distro (s)
  - Define (and build?) a set of SDV distros, including SDV projects, potentially demonstrating the starting points

## Meeting notes

You can access the meeting notes [here](meeting-notes/).

# SDV Showcases
These show cases are "running" examples that demonstrate the capabilities and features of software provided by SDV projects. The details of each showcase is provided in their respective pages

## Fleet Ops
| Truck Fleet Ops| |
|---|---|
|Short Summary | A close to "real-life" showcase for truck fleet management where trucks run SDV stacks so that logistics fleet operators can manage apps, data and services for a diverse set of vehicles.|
|What is in the showcase | Data collection from CANBus, In vehicle data brokers, VSS Signal Specification, FMS servers, Apps and Services using this data|
|SDV Projects Involved| Leda, Kuksa, Velocitas, Charriot, SommR?|
|Other interesting Technologies|InfluxDB, Prometheus, Eclipse Hono, Eclipse Kanto, Digital.Auto |
| Architecture Overview | ![FleetArchSDV.png](./meeting-notes/FleetArchSDV.png)|
| Distro | TBD|
| Coordinator | Kai Hudalla |


## ROS Racer
| ROS Racer | |
|---|---|
|Short Summary | A ROS based showcase where autonomous racers that run F1Tenth stacks that are orchestrated and managed by SDV |
|What is in the showcase | Orchestration of ros software on racers and data collection from vehicles, providing remote control, race Apps and Services using SDV stacks|
|SDV Projects Involved| Muto, Leda, Charriot, Kuksa?, Velocitas?|
|Other interesting Technologies| ROS, Kanto, Eclipse Ditto|
| Architecture Overview | ![ROSRacer.png](./meeting-notes/ROSRacerArchitecture.png)|
| Distro | TBD|
| Coordinator | Naci Dai |


## Insurance and Data Exchange 

| Insurance ||
|---|---|
| Short Summary | A showcase for integrating Eclipse Software Defined Vehicle (SDV) and Eclipse Dataspaces for insurance applications. Enable exchange of vehicle and risk events to support risk assesment, underwriting processes and customer experiences  |
| What is in the showcase | Identification of common driver risk events (maneuver detection) such as tailgaiting or speeding using CAN bus signals. Data capture / snapshotting and transmission of risk events to a telemetics platform using MQTT pub. Data storage in a Insurance Landing Zone. Data exchange using Eclipse Dataspace components between two entities representing the OEM and the Insurance Platform |
| SDV Projects Involved | Charriot, Ibeji, Kuksa? |
| Other interesting Technologies | Eclipse Dataspace Components, MQTT |
| Architecture Overview | ![InsuranceSDVEDC.png](./use-cases/insurancesdvedc/assets/InsuranceSDVEDC.png)|
| Distro | TBD|
| Coordinator | Mario Ortegon |


[Insurance and Data Exchange main page](./use-cases/insurancesdvedc/README.md)

# SDV Distros

What is a distro? A distro is a collection of software components built, assembled and configured so that it can essentially be used "as is" ([Wikipedia](https://en.wikipedia.org/wiki/Software_distribution)). It is often the closest thing to turnkey form of free software. A distro may take the form of a binary distribution, with an executable installer which can be downloaded from the Internet. Examples range from whole operating system distributions to server and interpreter distributions (for example WAMP installers).

An SDV distribution is a versioned set of artifacts/packages/images from participating SDV projects. The SDV Usecases should demonstrate how these distros can be used using some practical examples.

## Capabilities and SDV Landscape

SDV distros are described by a set of [capabilities - not yet defined]() that are derived from its components. These capabilities may be related to th SDV landscape (under construction).

## Why?

The purpose of the SDV distributions is to provide SDV starting points, and let developers work against a relatively easy to access and stable codebase until they are ready to roll everything forward. If SDV projects in a distro depend on each other once a distribution is released, these projects should attempt to limit changes in that release to bug fixes and non-breaking improvements. Of course, these rules are not strict, and it is the responsibility of the maintainers of a given project to avoid breaking changes.

## What?

Not all SDV projects make sense together and share a common "artifact" model. There are many different types of SDV "Apps" with different needs, and should anticipate that parts of the SDV community may put together their specific distributions to better target these platforms. For example a distro may include container images, python packages, API versions, Linux distros, Recipes that create working systems, etc.

Some distributions may target only certain environments, such as device runtime vs development environment. The latter may consist of IDE Add-Ons, DevContainers and SDKs rather than bootable system images for devices. Also the lifecycle of these components may be coupled and decoupled from each other.

Possible way of defining SDV Distributions:

| SDV Distro | Distro Type | Capabilities / Components | Preinstalled example use cases | Link | Target Platform(s) |
|---|---|---|---|---|---|
| Leda Quickstart | Bootable system image (Yocto) | Self Update, Container Update, Vehicle Signals Databroker | Velocitas: Seat Adjuster | [Releases](https://github.com/eclipse-leda/leda-distro/releases) | Edge Device (QEMU, Raspberry Pi, Docker, ARM-64, x86_64) |
| Distro B | rosdistro | Muto: Telemetry, LiveUI, ... | F1Tenth simulator | | ROS2, Edge Device and Development Environment |
| Distro C | VSCode DevContainer | Development Tooling: eCAL, Eclipse ArchE, ... | ... | ... | Docker |
| Distro D | ... | ... | ... | ... | ... |
| Distro E | ... | ... | ... | ... | ... |

>> _Comment from Mike Haller_: This is a more technical way and adheres to the "Not all SDV projects make sense together". At least, it would define some kind of high-level product description.

Another way of defining SDV Distributions is more like in a shared release planning:

| SDV Distro | Capabilities  | Components | Release Plan Link | 
|---|---|---|---|
| SDV Release 2023.1 | A user (developer) can implement a Vehicle App and deploy it to a device runtime | Leda 0.1<br>Muto 0.2<br>Velocitas 0.3<br>Kuksa 0.4 | [Release Plan](...)
| SDV Release 2023.2 | An embedded developer can migrate a simple AutoSAR application to the Velocitas programming model | Leda 0.4<br>Velocitas 0.3<br>Kuksa 0.5<br>Muto 0.2 | [Release Plan](...)
| SDV Release 2023.3 | Device runtimes can run arbitrary ROS stacks | Muto 0.3<br>Leda 0.4<br>Velocitas 0.3<br>Kuksa 0.5 | [Release Plan](...)
| SDV Release 2023.4 | Vehicle-2-X use cases are now fully supported | Muto 0.3<br>Leda 0.4<br>Velocitas 0.3<br>Kuksa 0.5 | [Release Plan](...)

>> _Comment from Mike Haller_: Where I see the value here is in a common release train. That really requires us to align much more, even if the individual components don't technically share something with each other. It will push the projects without a release yet to do a release

## When?

The first SDV Distro is targeted for the community day event in March,2023.  

For the long term, many Eclipse projects participate in Eclipse Foundation's quarterly simultaneous release. It might make sense for SDV Distro to follow a similar schedule, and at some point participate in the release train.

>> _Comment from Mike Haller:_ The EF doesn't do quarterly simultaneous releases. The Eclipse IDE project does though, which is quite a large project ecosystem already, and is targeted as a single, logical product. Do we strive for such kind of product release for SDV as well, e.g. include device runtimes and development-time tooling and specification projects also into an "SDV Release Train" ?

## How?

Ideally, the process for building distros should be captured in manifests that are self-explanatory, self-service and automatable.

We may capture the components that make up a distribution for example in our rosdistro format ([Example rosdistro file](https://github.com/ros/rosdistro/blob/master/galactic/distribution.yaml)), as it allows for multiple distributions.

It is proposed that the [Eclipse Leda]( https://projects.eclipse.org/projects/automotive.leda) project will provide the recipes ([Example BitBake Recipe](https://github.com/eclipse-leda/meta-leda/blob/main/meta-leda-components/recipes-sdv/packagegroups/packagegroup-sdv-core.bb)) for a base distribution focused on bootable system images for edge devices (eg not for development SDKs).

>> _Comment from Mike Haller:_ I'm a bit unhappy to already propose the manifest formats here at this point (e.g. BitBake Recipes vs. rosdistro file format), as we have so many different types of components in SDV already, targeting different aspects (e.g. build time, runtime, device vs. developer environment) that it feels to early to decide on this. Also, i am not sure what kind of problem we try to solve with it.  Also, these files are specific to the build or runtime (rosdistro requires a package.xml to be there, the BitBake recipes require a BitBake build process).

>> It does not make sense to me yet, as we don't even have a common artifact source (sources from git, docker containers)

There should be a smoke-testing process to make sure that distros and their targeted use-cases are in working condition.
# Other Potential Showcases/Usecases

The following are some existing, or potenial canidates forusecases that are relevant to SDV.  Some of them were implemented by hack teams during the [BCX Hackathon (BCX 2022)](https://eclipse-sdv-hackathon-bcx.github.io/.github/) [challenges](https://eclipse-sdv-hackathon-bcx.github.io/.github/) presented potential candidates, also the following suggestions were proposed:

## Truck fleet management (Daniel Krippner)

Truck fleet management where trucks run SDV stacks so that logistics fleet operators can manage a diverse set of vehicles by having their fleet operator service supplier install data collection clients and other related features into the trucks, as 3rd party SDV-style applications.

## Dynamic car insurance (Daniel Krippner)

Dynamic ca insurance where insurance companies offer rates that dynamically adapt to driving behavior, implemented via 3rd party SDV-style data collection/evaluation applications that run on SDV stacks in customer vehicles.

## Driving Score Challenge (BCX 2022)

Improve the quality of a driver's driving style. The use case is well-known, there are already some commercial offerings available. As a hack challenge for Eclipse SDV, it's a good use case to try out the involved technology and experience some of the challenges when dealing with connected vehicles. You will also get to know a few more open source projects along the way.

## Passenger Welcome (BCX 2022)

The idea of the 'Passenger Welcome' use case is that the driver (or a passenger) is detected while he is approaching the vehicle. Based on this information, your Welcome Vehicle Application could control a sequence of events, such as welcoming the passenger with a special lighting sequence (using lights outside and/or inside the car), automatically opening the door or trunk, adjusting the seat position according to driver preferences, the same for steering wheel position, cabin temperature or air condition, etc. – there is no limitation to the creativity in this hackathon. What comes to your mind about how your own vehicle or a carsharing car should behave when a passenger approaches?

## Hack the Truck (BCX 2022)

The the truck features a set of sensors and actuators that you can work with. Everything in the truck is connected to some PCs. The PCs are connected to each other with a regular ethernet switch. For unifying the communication, our middleware Eclipse eCAL™ is used. So, for receiving sensor data in your application and controlling actuators you only need to connect your notebook to the same switch.

## Virtual Vehicle Application with Simulation (BCX 2022)

As a hack challenge for Eclipse SDV, you will have the opportunity to get involved with the technology and experience some of the challenges when dealing with connected vehicles and build the next generation of software defined vehicles. You will also get to know a few more open source projects along the way. For this hackchallenge we considered 2 simulation tools, Carla and AirSim although you are not restricted to only these. If you have experience with other simulation tools and have a great idea on how to use it in this hackchallenge please do so.

## Control vehicle lights (BCX 2022)

Our Retro-suitcase houses a VW T2 light retrofitted with a SOME/IP ECU (Electronic Control Unit). Another board runs Eclipse SommR daemon providing access to the service for hack participants. Use Eclipse KUKSA.val databroker to control the lights or read their state. Alternatively, the service can be accessed directly from Rust, Java or Kotlin applications. Our hack challenge is intended to complement other hack challenges, for example Passenger welcome or Driving Score. For example, you can add blinking sequences to these challenges. Since we use KUKSA.val databroker as a mediator, additional effort to access the lights is minimal.

## OpenStreetMap and Carla (BCX 2022)

As a SDV developer I want to put my software into a car that drives any specific route in any specific city with an effort comparable programming a navigation system like google maps. The rough plan is to extract a city from OSM, convert into importable format for the simulator (provided by the simulator). A route on the imported virtual world is created and navigation steps from a routing engine navigates a virtual vehicle on the fastest way in the virtual world:

- show how real world data can be brought into a virtual world running on a simulator
- show how vehicle can navigate from A to B in a virtual world using technology and experience from open source projects
