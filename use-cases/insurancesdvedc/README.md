# Connected Insurance with Eclipse Software Defined Vehicle and Eclipse Dataspace Components

# Summary
This document outlines a blueprint for integrating Eclipse Software Defined Vehicle (SDV) and Eclipse Dataspaces into insurance applications. By combining SDV's approach to vehicle software and Dataspaces' data management capabilities, insurers can enhance risk assessment, underwriting processes, and customer experiences.

# Description
The following diagram shows a high level overview of the components of the Blueprint:
 
![InsuranceSDVEDC.png](./assets/InsuranceSDVEDC.png)

The *maneuver detection module* consist of a collection of *event detectors*. Each detector monitors a number of signals (for example, speed, brake pedal position or steering wheel angle) at high resolution. When the monitor is triggered, it collects relevant signals associated to the event from the circular buffer. The event detector creates an *event payload* with additional metadata information. A cloud connector transports the event to the telematics platform. 
The *telematics platform* (representing a vehicle OEM) stores all insurance events in an *insurance landing zone*. An Eclipse Dataspace component provides a catalog that describes both aggregated and individual datasets. A second Eclipse Dataspace component, representing the insurance company, uses the catalog to discover relevant signals and can connect to the event stream.

# Tasks
The blueprint consists of the following tasks:

1.	Integrate the relevant Eclipse Projects
    1.	Application Programing Model
    1.	Digital Twins
    1.	Vehicle Data Stream
1.	Create a basic recorder implementation as a capability that is exposed through the application programming model
1.	Create the Event Detector Framework, consuming signal values from the in-vehicle digital twin. The event detector framework consumes capabilities from the application programming model and uses the digital twin to discover the availability of relevant signls
1.	Create Sample Event Detectors based on the most common accident causes
a.	Select signals and thresholds relevant for each type of event
1.	Create a definition for the event payload
1.	Create  recordings with sample data according to the COVESA Vehicle Signal Specification
1.	Structure and store the information in an insurance landing zone, in preparation for sharing
1.	Create a deployment of an EDC representing the OEM telematics platform based on the Minimum Viable Dataspace
    a.	Define a basic catalog that describes the data
    b.	Define basic / sample policies and trust
1.	Create a deployment of an EDC representing the Insurance Platform as a consumer of the data

# Additional considerations
Transmission of data to the cloud implementation must be in common with other Eclipse Projects.

# Rationale

* Moving the risk maneuver detection to the edge increases the precision of the detection and reduces the amount of data transmitted from the vehicle to the cloud. 
* The usage of dataspaces allows OEM to remain in control of data sharing and remain in full compliance of current and upcoming EU legislation.
* The event risks themselves are a non differnciating element - differenciation is on the agreed upon amount of detail that OEMs want / can provide, and on the risk scoring algorithms used internally by insurance companies.


# Examples

The following table shows some examples for event detectors:


| Event | Monitor Signal | Capture |
| ----- | -------------- | ------- |
| Speeding |	Speed, Speed Limit	Position | Speed, Speed Limit (From Navigation System), Wheel position sensor, accelerator pedal position, brake pedal position |
| Reckless Driving | Accelerometer (Harsh Breaking, Harsh Acceleration, Harsh Maneuver) | Accelerator Pedal Position, Brake Pedal Position, Lateral and Longitudinal Acceleration, Position |
| Rain, | Rain Detection | Rain Detection Start / End, Intensity, Position |
| Unsafe Lane Changes | Wheel position sensor, Speed, Turn Indication, ADAS – Lane Departure Warning |	Steering Wheel position sensor, Speed, Turn Indication, ADAS – Lane Departure Warning, Obstacle,  Positions |
| Tailgaiting | Obstacle position in front, Speed | Obstacle position in front, Speed |

# References
* [Eclipse Minimum Viable Dataspace project](https://github.com/eclipse-edc/MinimumViableDataspace)

