# Ideation
The intention of this list to collect an ideation of potential blueprints. 

The inital ideation was part of the Eclipse SDV Community Day in Friedrichshafen on 06th July 2023:

# New blueprints

## Custom Over the Air Updates 

The idea is to have a blueprint which deals with over-the-air (OTA) updates of software and configuration in the vehicle. There was the explicit wish to not only focus on "classic" OTA, where one rolls-out the same software version to many vehicles, but also to include a scenario where the configuration of specific vehicles, e.g. for testing, can be adapted.

## De-centralization through Zenoh, Kuksa, uProtocol

In the Eclipse SDV space we have multiple projects which deal with abstracting the discovery and definition of vehicle sensing and actuation interactions. Examples are Eclipse Zenoh, Eclipse uProtocol, and Eclipse Kuksa where the first two are explicitly tailored towards distributed scenarios where part of the computation can happen outside a single vehicle. We could therefore come up with a use case which by its nature requires data from different and distributed data sources either from different domains in a single vehicle or even from multiple vehicles and other entities in the system. 

Adding Eclipse Kuksa is also interesting here, since one idea for the Eclipse Kuksa.val data broker is to act as the main entry and interaction point between applications and more deeply embedded layers in a vehicle. Such a blueprint is a way to explore the integration options between Eclipse Kuksa.val and distributed architectures where Kuksa.val then acts as one of potentially multiple data sources. 

## eCAL, Eclipse SDV Developer Console 

There was mutual interest by members of the Eclipse SDV Developer Console team and the Eclipse eCal team to further discuss a potential joint blueprint.

## common measurement formats between eCAL and openMDM

There is potential to discuss common measuremen formats used by Eclipse eCAL and openMDM.

# Integration Ideas to existing Blueprints:

## Eclipse Ankaios as In-Vehicle Container Orchestrator for FMS Use Case

To give an example of an Eclipse Ankaios deployment, the in-vehicle setup for the fleet-management blueprint shall be done with Eclipse Ankaios. The initial setup of the fleet-management allows to deploy the containers for the In-Vehicle part either through Docker Compose or Eclipse Kanto container manifests. Adding Eclipse Ankaios descriptors is therefore a chance to compare the approaches in the same blueprint.

